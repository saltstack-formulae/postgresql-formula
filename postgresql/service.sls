{% from "postgresql/map.jinja" import postgresql with context %}

{% if not postgresql.patroni.enabled %}
postgresql-service:
  service.running:
    - name: postgresql.service
    - enable: True
{% endif %}
