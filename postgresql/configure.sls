{% from "postgresql/map.jinja" import postgresql with context %}
{% set pg_major = postgresql.version.split('+')[0] %}

{% if postgresql.server.config|length != 0 and not postgresql.patroni.enabled %}
postgresql_conf:
  file.managed:
    - name: /etc/postgresql/{{ pg_major }}/main/postgresql.conf
    - user: postgres
    - group: postgres
    - mode: "0644"
    - template: jinja
    - source: "salt://postgresql/files/postgresql.conf.j2"
{% endif %}

{% if postgresql.pg_hba.config|length != 0 and not postgresql.patroni.enabled %}
pg_hba_conf:
  file.managed:
    - name: /etc/postgresql/{{ pg_major }}/main/pg_hba.conf
    - user: postgres
    - group: postgres
    - mode: "0644"
    - template: jinja
    - source: "salt://postgresql/files/pg_hba.conf.j2"
{% endif %}

{% if postgresql.patroni.enabled %}
patroni_conf:
  file.serialize:
    - name: /etc/patroni/config.yml
    - user: postgres
    - group: postgres
    - mode: "0644"
    - makedirs: True
    - dataset: {{ postgresql.patroni.config }} 
    - serializer: yaml
{% endif %}
