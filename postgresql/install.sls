{% from "postgresql/map.jinja" import postgresql with context %}

postgres:
  group.present:
    - system: True
  user.present:
    - fullname: PostgreSQL administrator
    - shell: /bin/bash
    - home: /var/lib/postgresql
    - gid: postgres
    - system: True
    - require:
      - group: postgres

{% if postgresql.server.encrypt_data_dir.enabled %}
postgresql_ecryptfs_packages:
  pkg.installed:
    - pkgs:
      - ecryptfs-utils: latest

postgresql_ecryptfs_dir:
  file.directory:
    - name: /var/lib/.postgresql
    - user: root
    - group: root

postgresql_ecryptfs_mountpoint:
  file.directory:
    - name: /var/lib/postgresql
    - user: postgres
    - group: postgres
    - require:
      - user: postgres

postgresql_mount_encrypted_data_dir:
  cmd.run:
    - name: echo passphrase_passwd=$passphrase | mount -t ecryptfs -o ecryptfs_enable_filename_crypto=n -o ecryptfs_passthrough=n -o no_sig_cache=y -o ecryptfs_unlink_sigs -o ecryptfs_key_bytes=16 -o ecryptfs_cipher=aes -o key=passphrase:passphrase_passwd_fd=0 /var/lib/.postgresql /var/lib/postgresql
    - unless: mount| grep -q /var/lib/.postgresql
    - hide_output: True
    - output_loglevel: quiet
    - env:
      - passphrase: {{ postgresql.server.encrypt_data_dir.passphrase }}
    - require:
      - file: postgresql_ecryptfs_dir
      - file: postgresql_ecryptfs_mountpoint

{% endif %}

postgresql-server:
  pkg.installed:
    - pkgs:
      - postgresql: {{ postgresql.version }}
      - postgresql-client: {{ postgresql.version }}
      - postgresql-contrib: {{ postgresql.version }}
      - pgbouncer: {{ postgresql.pgbouncer.version }}
{% if postgresql.server.encrypt_data_dir.enabled %}
    - require:
      - cmd: postgresql_mount_encrypted_data_dir
{% endif %}

{%- if postgresql.patroni.enabled %}
patroni-pip-packages:
  pip.installed:
    - pkgs:
      - python-consul
      - psycopg2-binary
      - psycopg>=3.0
      - patroni
{%- endif %}
